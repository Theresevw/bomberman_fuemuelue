import os
import pickle
import random

from collections import namedtuple
import numpy as np
import scipy
from collections import namedtuple
from random import shuffle

import settings as s

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

set_policy = namedtuple('set_policy', ('name', 'train_param', 'play_param'))
POLICY = set_policy(name = 'SOFTMAX', train_param = 20, play_param = 1)
NO_BOMBING = False
COOLING = False
MODEL_UPDATE_PERIOD = 500

def setup(self):
    """
    This is called once when loading each agent.
    Make sure that you prepare everything such that act(...) can be called.

    When in training mode, the separate `setup_training` in train.py is called
    after this method. This separation allows you to share your trained agent
    with other students, without revealing your training code.


    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    if self.train:
        #print chosen hyperparameters
        print("Training is turned on with policy: ", POLICY.name)
        if POLICY.name == 'EPSILON_GREEDY':
            print("Epsilon for training is: ", POLICY.train_param)
        elif POLICY.name == 'SOFTMAX':
            print("Temperature for training is:", POLICY.train_param)
            self.COOL_DOWN=POLICY.train_param

    else:
        #print chosen hyperparameters
        print("Play with policy: ", POLICY)
        if POLICY.name == 'EPSILON_GREEDY':
            print("Epsilon for this game is: ", POLICY.play_param)
        elif POLICY.name == 'SOFTMAX':
            print("Temperature for this game is:", POLICY.play_param)


        #check whether saved model exists
        if os.path.isfile("my-saved-model.pt"):
            print("Loading saved model to play.")
            with open("my-saved-model.pt", "rb") as file:
                self.model = pickle.load(file)
        else:   
            print("Error: not able to load my-saved-model.pt and training is not turned on")


    #initialize Q_t as self.Q, the Q guesses for each action for the current time step
    self.Q = np.zeros(len(ACTIONS)-NO_BOMBING )

    #introduce flag in order to change into play mode during training
    self.temporary_play_mode = False

def act(self, game_state: dict) -> str:
    """
    Your agent should parse the input, think, and take a decision.
    When not in training mode, the maximum execution time for this method is 0.5s.

    :param self: The same object that is passed to all of your callbacks.
    :param game_state: The dictionary that describes everything on the board.
    :return: The action to take as a string.
    """

    if POLICY.name == 'EPSILON_GREEDY' and os.path.isfile('my-saved-model.pt'):
        if self.train  and not self.temporary_play_mode:
            epsilon = POLICY.train_param
        else:
            epsilon = POLICY.play_param


        if random.random() < (1 - epsilon):

            compute_Q(self,game_state)
            return ACTIONS[np.argmax(self.Q)]

        else:
            #return np.random.choice(ACTIONS, p=[.25, .25, .25, .25, 0, 0])
            return np.random.choice(ACTIONS, p=[.17, .17, .17, .17, .16, .16])


    elif POLICY.name == 'SOFTMAX' and os.path.isfile('my-saved-model.pt'):
        if self.train and not self.temporary_play_mode:
            if COOLING:
                temperature=self.COOL_DOWN
            else:
                temperature = POLICY.train_param
            
            if (game_state['round'])%(MODEL_UPDATE_PERIOD)==0 and COOLING and game_state['step']==1:
                self.COOL_DOWN-=0.3
                print('current_temperature: ', temperature)
        else:
            temperature = POLICY.play_param

        compute_Q(self, game_state)
        probabilities = scipy.special.softmax(self.Q / temperature)
        return ACTIONS[np.random.choice(np.arange(len(ACTIONS)-NO_BOMBING), p=probabilities)]


    else:
        return np.random.choice(ACTIONS, p=[.17, .17, .17, .17, .16, .16])



def compute_Q(self, game_state):
    X_t = state_to_features(game_state)

    #print("compute Q in step", game_state['step'])
    for i in range(len(ACTIONS)-NO_BOMBING):
        self.Q[i] = self.model[i].predict(X_t.reshape(1,len(X_t)))[0]


def nearest_target(position, targets):
    """Find neares target relative to position of agent
    
    :param position: the coordinate from which to begin the search.
    :param targets: a list or array holding the coordinates of all target tiles
    
    :return: coordinate of closest target.
    """
    if len(targets) == 0: return (0,0)
    distance = np.sum(np.abs(np.subtract(targets, position)), axis=1)
    nearest = np.argmin(distance)
    
    return (targets[nearest][0]-position[0], targets[nearest][1]-position[1])
    
   


def state_to_features(game_state: dict) -> np.array:
    """
    Converts the game state to the input of your model, i.e.
    a feature vector.

    You can find out about the state of the game environment via game_state,
    which is a dictionary. Consult 'get_state_for_agent' in environment.py to see
    what it contains.

    :param game_state:  A dictionary describing the current game board.
    :return: np.array
    """
     # This is the dict before the game begins and after it ends
    if game_state is None:
        return None

    # get information about the game state
    field = game_state['field']
    _,_,bombs_left, (x_self, y_self) = game_state['self']
    coins = game_state['coins']
    crates = [(x,y) for x in range(1,16) for y in range(1,16) if (field[x,y] == 1)]
    dead_ends=[(x,y) for x in range(1,16) for y in range(1,16) if (field[x,y] == 0 and np.sum([abs(field[d]) for d in directions(x,y)])==3)]
    others=[(x,y) for (_,_,_,(x,y)) in game_state['others']]
    others_bomb=[b for (_,_,b,(x,y)) in game_state['others']]
    bombs = game_state['bombs']

    feature_list = []

    #level of danger plus 'do not run into a current explosion'
    map_of_danger=get_map_of_danger(field, game_state['explosion_map'], bombs, dead_ends, others)#+6*game_state['explosion_map']
    
    # Add to the features the level of danger of the neighbour tiles (top, bottom, left, right) 
    for d in directions(x_self,y_self):
        feature_list.append(map_of_danger[d])
    
    #print(map_of_danger)

    # Find coordinates of the closet targets relative to position of the agent
    targets = []
    targets.append(nearest_target((x_self,y_self), coins))
    targets.append(nearest_target((x_self,y_self), crates))
    targets.append(nearest_target((x_self,y_self), others))
   

    # Find relative coordinates of targets 
    for target in targets:
        feature_list.append(target[0])
        feature_list.append(target[1])

    # append other bomb flag (True, if any other is currently bombing)
    """feature_list.append(0)
    if True in others_bomb:
        feature_list[-1] = 1"""
    
    # Append other flag (True, if other is neigbour of agent)
    feature_list.append(0)
    for d in directions(x_self,y_self):
        if d in others:
            feature_list[-1] = 1

    # append current danger (how explosive is the the position of the agent)
    feature_list.append(map_of_danger[x_self,y_self])

    # Append the bomb flag
    feature_list.append(not bombs_left)

    # Append crater flag (True, if crater is neigbour of agent)
    feature_list.append(0)
    for d in directions(x_self,y_self):
        if field[d] == 1:
            feature_list[-1] = 1
            
    # concatenate them as a feature tensor 
    stacked_channels = np.stack(feature_list)
    
    # and return them as a vector
    return stacked_channels.reshape(-1)


def directions(x,y):
    """
    :param x: x coordinate
    :param y: y coordinate

    :return list of reachable tiles from the current position (x,y)
    """
    return [(x, y-1), (x, y+1), (x-1, y), (x+1, y)]



def get_map_of_danger (field, explosion_map, bombs, dead_ends, others=[]):
    """ 
    Converts the informations of the current game_state in a map, which indicates how danger 
    a tile of the fields is, with regard to threads from current bombs.
    The closer the bomb is, the higher is the danger on that tile. 
    To avoid running into dead ends, which are still close enough to be in the line of fire of the bomb, the 
    danger level of such tiles, which are part of a dead end, is decreased

    :param field: the field of the game_state, including stones and crates
    :param bombs: list of bombs of the game_state
    :param dead_ends: list of dead ends
    :param others: 

    :return: np.array, a map which indicates the danger level of tiles. 
            The higher the danger level is the more the agent should avoid the corresponding tile  
    """
  
    if len(bombs)<=0:
        return 5*np.abs(field)
    
    future_explosion_map = np.zeros((s.COLS, s.ROWS))
    dead_end_map = np.zeros((s.COLS, s.ROWS))
       
    
    for bomb in bombs:

        (x_b, y_b), t = bomb
        
        #inner bracket: potential bombing fields
        bomb_range_left = np.argmin(np.flip( field[max(0,(x_b - s.BOMB_POWER)) : 1 + x_b, y_b])) - 1
        bomb_range_right = np.argmin(field[x_b : 1 + (x_b + s.BOMB_POWER), y_b]) - 1
        bomb_range_down = np.argmin(np.flip(field[x_b, max(0,(y_b - s.BOMB_POWER)) : 1 +  y_b])) - 1
        bomb_range_up = np.argmin(field[x_b, y_b :1 + (y_b + s.BOMB_POWER)]) - 1

        #actual bombing fields
        if bomb_range_left == -1:
            bomb_range_left = s.BOMB_POWER
        if bomb_range_right == -1:
            bomb_range_right = s.BOMB_POWER
        if bomb_range_down == -1:
            bomb_range_down = s.BOMB_POWER
        if bomb_range_up == -1:
            bomb_range_up = s.BOMB_POWER
        
        #for bombing fields: add closeness to the bomb: the closer to the bomb, the higher is danger
        future_explosion_map[x_b - bomb_range_left : x_b, y_b] = (np.arange(bomb_range_left)+1)
        future_explosion_map[x_b+1 :x_b+ bomb_range_right+1 , y_b] = (np.flip(np.arange(bomb_range_right))+1)
        future_explosion_map[x_b, y_b - bomb_range_down: y_b] = (np.arange(bomb_range_down)+1)
        future_explosion_map[x_b, y_b+1: y_b + bomb_range_up+1 ] = (np.flip(np.arange(bomb_range_up))+1)
        future_explosion_map[x_b,y_b]=(1+max(bomb_range_left,bomb_range_right,bomb_range_up,bomb_range_down))


        # consider others like crates
        for (x,y) in others:
            field[x,y]=1

        # potential dead end ranges
        dead_range_left = np.argmax(np.flip(np.abs(field[max(0,(x_b - s.BOMB_POWER-1)) : 1 + x_b, y_b]))) - 1
        dead_range_right = np.argmax(np.abs(field[x_b : 1 + (x_b + s.BOMB_POWER+1), y_b])) - 1
        dead_range_down = np.argmax(np.flip(np.abs(field[x_b, max(0,(y_b - s.BOMB_POWER-1)) : 1 +  y_b]))) - 1
        dead_range_up = np.argmax(np.abs(field[x_b, y_b :1 + (y_b + s.BOMB_POWER+1)])) - 1
        
        # actual dead end ranges
        if dead_range_left == -1 or (x_b-dead_range_left,y_b) not in dead_ends:
            dead_range_left = 0
        if dead_range_right == -1 or (x_b+dead_range_right,y_b) not in dead_ends:
            dead_range_right = 0
        if dead_range_down == -1 or (x_b,y_b-dead_range_down) not in dead_ends:
            dead_range_down = 0
        if dead_range_up == -1 or (x_b,y_b+dead_range_up) not in dead_ends:
            dead_range_up = 0

        ## if the agent can still escape on the way to an dead end, do not forbit to walk in that direction
        # Therfore increase the inner_dead_range, the higher the inner dead range, the further away the dead end starts
        # relative to the bomb position
        outer_dead_range_left, inner_dead_range_left=dead_range_left,dead_range_left
        outer_dead_range_right, inner_dead_range_right=dead_range_right,dead_range_right
        outer_dead_range_down, inner_dead_range_down=dead_range_down,dead_range_down
        outer_dead_range_up, inner_dead_range_up=dead_range_up,dead_range_up

        x,y=x_b-dead_range_left, y_b
        while x<x_b:
            if (np.abs(field[x,y+1])==0 or np.abs(field[x,y-1])==0):
                inner_dead_range_left=dead_range_left
            dead_range_left-=1
            x+=1
        if inner_dead_range_left==outer_dead_range_left:
            inner_dead_range_left=0

        x,y=x_b+dead_range_right, y_b
        while x>x_b:
            if (np.abs(field[x,y+1])==0 or np.abs(field[x,y-1])==0):
                inner_dead_range_right=dead_range_right
            dead_range_right-=1
            x-=1
        if inner_dead_range_right==outer_dead_range_right:
            inner_dead_range_right=0

        x,y=x_b, y_b-dead_range_down
        while y<y_b:
            if (np.abs(field[x+1,y])==0 or np.abs(field[x-1,y])==0):
               inner_dead_range_down=dead_range_down
            dead_range_down-=1
            y+=1
        if inner_dead_range_down==outer_dead_range_down:
            inner_dead_range_down=0
       
        x,y=x_b,y_b+dead_range_up
        while y>y_b:
            if (np.abs(field[x+1,y])==0 or np.abs(field[x-1,y])==0):
                inner_dead_range_up=dead_range_up
            dead_range_up-=1
            y-=1
        if inner_dead_range_up==outer_dead_range_up:
            inner_dead_range_up=0

       
        dead_end_map[x_b- outer_dead_range_left : x_b-inner_dead_range_left, y_b] = np.flip(np.arange(outer_dead_range_left-inner_dead_range_left)+2)
        dead_end_map[x_b+1+inner_dead_range_right :x_b+1+outer_dead_range_right , y_b] = ((np.arange(outer_dead_range_right-inner_dead_range_right))+2)
        dead_end_map[x_b, y_b-outer_dead_range_down: y_b-inner_dead_range_down] = np.flip(np.arange(outer_dead_range_down-inner_dead_range_down)+2)
        dead_end_map[x_b, y_b+1+inner_dead_range_up: y_b+outer_dead_range_up+1 ] = ((np.arange(outer_dead_range_up-inner_dead_range_up))+2)
        dead_end_map[x_b,y_b]=0
             
    
    #map of danger icludes: crates, stones, dead ends and closeness to a bomb
    map_of_danger= future_explosion_map + 5*explosion_map + 5*np.abs(field) + dead_end_map

    return map_of_danger

