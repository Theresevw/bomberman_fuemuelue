import os
import pickle
import random
from collections import namedtuple

import numpy as np
import scipy


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

set_policy = namedtuple('set_policy', ('name', 'train_param', 'play_param'))

# for EPSILON_GREEDY policy:
# param = epsilon
#POLICY = set_policy(name = 'EPSILON_GREEDY', train_param = 0.5, play_param = 0.05)

# for SOFTMAX policy:
# param = temperature, 0.1 is equivalent to argmax #5 leads to 2 actions ca 40 % each
POLICY = set_policy(name = 'SOFTMAX', train_param = 20, play_param = 1)


def setup(self):
    """
    This is called once when loading each agent.
    Make sure that you prepare everything such that act(...) can be called.

    When in training mode, the separate `setup_training` in train.py is called
    after this method. This separation allows you to share your trained agent
    with other students, without revealing your training code.


    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    if self.train:
        #print chosen hyperparameters
        print("Training is turned on with policy: ", POLICY.name)
        if POLICY.name == 'EPSILON_GREEDY':
            print("Epsilon for training is: ", POLICY.train_param)
        elif POLICY.name == 'SOFTMAX':
            print("Temperatue for training is:", POLICY.train_param)


    else:
        #print chosen hyperparameters
        print("Play with policy: ", POLICY)
        if POLICY.name == 'EPSILON_GREEDY':
            print("Epsilon for this game is: ", POLICY.play_param)
        elif POLICY.name == 'SOFTMAX':
            print("Temperatue for this game is:", POLICY.play_param)


        #check whether saved model exists
        if os.path.isfile("my-saved-model.pt"):
            print("Loading saved model to play.")
            with open("my-saved-model.pt", "rb") as file:
                self.model = pickle.load(file)
        else:   
            print("Error: not able to load my-saved-model.pt and training is not turned on")

    #initialize Q_t as self.Q, the Q guesses for each action for the current time step
    self.Q = np.zeros(len(ACTIONS) - 2)


def act(self, game_state: dict) -> str:
    """
    Your agent should parse the input, think, and take a decision.
    When not in training mode, the maximum execution time for this method is 0.5s.

    :param self: The same object that is passed to all of your callbacks.
    :param game_state: The dictionary that describes everything on the board.
    :return: The action to take as a string.
    """
    # TODO: Bombing and waiting is completely ignored for now.

    if POLICY.name == 'EPSILON_GREEDY' and os.path.isfile('my-saved-model.pt'):
        if self.train:
            epsilon = POLICY.train_param
        else:
            epsilon = POLICY.play_param

        if random.random() < (1 - epsilon):

            compute_Q(self, game_state)
            return ACTIONS[np.argmax(self.Q)]

        else:
            return np.random.choice(ACTIONS, p=[.25, .25, .25, .25, 0, 0])


    elif POLICY.name == 'SOFTMAX' and os.path.isfile('my-saved-model.pt'):
        if self.train:
            temperature = POLICY.train_param
        else:
            temperature = POLICY.play_param

        compute_Q(self, game_state)
        probabilities = scipy.special.softmax(self.Q / temperature)
        return ACTIONS[np.random.choice(np.arange(len(ACTIONS) - 2), p=probabilities)]


    else:
        return np.random.choice(ACTIONS, p=[.25, .25, .25, .25, 0, 0])


def compute_Q(self, game_state):
    X_t = state_to_features(game_state)

    for i in range(len(ACTIONS) - 2):
        self.Q[i] = self.model[i].predict(X_t.reshape(1, len(X_t)))[0]


def state_to_features(game_state: dict) -> np.array:
    """
    *This is not a required function, but an idea to structure your code.*

    Converts the game state to the input of your model, i.e.
    a feature vector.

    You can find out about the state of the game environment via game_state,
    which is a dictionary. Consult 'get_state_for_agent' in environment.py to see
    what it contains.

    :param game_state:  A dictionary describing the current game board.
    :return: np.array
    """
     # This is the dict before the game begins and after it ends
    if game_state is None:
        return None



    #position agents 
    x_self = game_state['self'][3][0]
    y_self = game_state['self'][3][1]
    
    #calculate the distance to every coin on the field
    if len(game_state['coins']) >= 1:
        [x_coins, y_coins] =  np.stack(game_state['coins'],axis=-1)
        #y_coins =  game_state['coins'][:][1]

    
    #calculate coordinates of nearest coin
        distance_coins = (x_coins-x_self*np.ones(len(x_coins)))**2 + (y_coins-y_self*np.ones(len(y_coins)))**2
        nearest = np.argmin(distance_coins)
        x_nearest = x_coins[nearest]
        y_nearest = y_coins[nearest]
        
    
    #TODO: assign features a value that does not influence robot when no coins left
    else:
        x_nearest = 17
        y_nearest = 17
        
    relative_x = x_nearest - x_self
    relative_y = y_nearest - y_self
    
    #print("xself:",x_self,"xcoin nearest ",x_nearest)    
    #print("yself:",y_self,"ycoin nearest ",y_nearest)   
    
    

    
    # create feature list
    feature_list = []
    
    # store only the relative position between the agent and the next coin
    feature_list.append(relative_x)
    feature_list.append(relative_y)
    #check for possible moves
    feature_list.append(game_state['field'][x_self][y_self+1])
    feature_list.append(game_state['field'][x_self+1][y_self])
    feature_list.append(game_state['field'][x_self][y_self-1])
    feature_list.append(game_state['field'][x_self-1][y_self])
    #give own position on board
    #feature_list.append(x_self)
    #feature_list.append(y_self)

    # concatenate them as a feature tensor (they must have the same shape), ...
    stacked_channels = np.stack(feature_list)
    # and return them as a vector
    return stacked_channels.reshape(-1)
