import pickle
import random
from collections import namedtuple, deque
from typing import List

import numpy as np
import os
from sklearn.ensemble import GradientBoostingRegressor


import settings as s
import events as e
from .callbacks import state_to_features


# named tuple to store the game history
History = namedtuple('History',
                        ('X_t', 'i_action', 'X_tplus1', 'reward', 'game_state'))

# Hyper parameters
HISTORY_SIZE = 1  # keep only ... last transitions
#RECORD_ENEMY_TRANSITIONS = 1.0  # record enemy transitions with probability ...
GAMMA = 0.95
set_gradient_boost = namedtuple('set_gradient_boost', ('learning_rate', 'n_estimators', 'max_depth'))
GBR_param = set_gradient_boost(learning_rate=0.1, n_estimators=100, max_depth=3)
LEARNING_RATE = 0.5

INCLUDE_SYMMETRIC_STATES = True
TEST_PERFORMANCE = True

# list of actions
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']



def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # store reward history in double ended queue
    self.history = deque(maxlen=HISTORY_SIZE)

    # Setup 6 dim array of lists to have training data for each action: X_train and Y_train
    self.X_train = np.empty(len(ACTIONS),dtype=np.object)
    self.X_train[:] = [],[],[],[],[],[]
    self.Y_train = np.empty(len(ACTIONS),dtype=np.object)
    self.Y_train[:] = [],[],[],[],[],[]

    # setup variables for performance
    self.coins_collected = 0
    self.performance = []

    if os.path.isfile("my-saved-model.pt"):
        print("Load old model for further training")
        with open("my-saved-model.pt", "rb") as file:
            self.model = pickle.load(file)

    elif os.path.isfile("initial-model.pt"):
        print("Load initial model for training")
        with open("initial-model.pt", "rb") as file:
            self.model = pickle.load(file)
    else:
        self.Q_initial = 1
        print("Start with Q_initial = ", self.Q_initial)
        
        
def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action: The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events: The events that occurred when going from  `old_game_state` to `new_game_state`
    """
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    # Idea: Add your own events to hand out rewards
    #if ...:
    #    events.append(PLACEHOLDER_EVENT)

    # state_to_features is defined in callbacks.py

    # at beginning of round, append new performance tuple
    if new_game_state['step'] > 1:
        #state s_t
        X_t = state_to_features(old_game_state) 
        
        #state s_t+1
        X_tplus1 = state_to_features(new_game_state)
        
        #reward r_t+1
        reward = reward_from_events(self, events) + auxiliary_rewards(X_t,X_tplus1)

        #index of actions
        i_action = ACTIONS.index(self_action)

        # save this step in history
        self.history.append(History(X_t, i_action, X_tplus1, reward, old_game_state))

        if old_game_state['step'] >= HISTORY_SIZE:
            # store the feature vector for the earliest state in history
            self.X_train[self.history[0].i_action].append(self.history[0].X_t)

            # compute n-step reward, equals r_t+1 for HISTORY_SIZE = 1
            reward_n_step = 0
            for k in range(HISTORY_SIZE):
                reward_n_step += self.history[k].reward * GAMMA**k

            # compute expected reward for earliest state in history
            if not os.path.isfile("my-saved-model.pt") and not os.path.isfile("initial-model.pt"):
                Y = reward_n_step + GAMMA * self.Q_initial
                self.Y_train[self.history[0].i_action].append(Y)
       
            else:
                if LEARNING_RATE < 1:
                    Q_t = self.model[self.history[0].i_action].predict(
                        self.history[0].X_t.reshape(1, len(self.history[0].X_t)))[0]
                    Y = (1 - LEARNING_RATE) * Q_t + LEARNING_RATE * (
                                reward_n_step + GAMMA ** HISTORY_SIZE * maximal_Q(self, self.history[0].X_tplus1))
                else:
                    Y = (reward_n_step + GAMMA ** HISTORY_SIZE * maximal_Q(self, self.history[0].X_tplus1))
                self.Y_train[self.history[0].i_action].append(Y)

            if INCLUDE_SYMMETRIC_STATES:
                append_training_data_of_symm_states(self, Y)

        if TEST_PERFORMANCE and e.COIN_COLLECTED in events:
            #print('collected')
            self.coins_collected += 1

 
def maximal_Q(self, X):
    Q = np.zeros(len(ACTIONS))
    for i in range(len(ACTIONS)-2):
        Q[i] = self.model[i].predict(X.reshape(1,len(X)))[0]
        
    return np.max(Q)
        
    
def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.

    This is similar to reward_update. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self: The same object that is passed to all of your callbacks.
    """
    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')
    #self.transitions.append(Transition(state_to_features(last_game_state), last_action, None, reward_from_events(self, events)))

    N_round = last_game_state['round']
    if TEST_PERFORMANCE:
       # print(self.coins_collected)
       # print('Score:',last_game_state['self'][1])
        self.performance.append(np.array([self.coins_collected, last_game_state['step']]))
        self.coins_collected = 0

    if N_round % 50 == 0 :
        print("update Q at round ", N_round)
        self.model = np.empty(len(ACTIONS), dtype=np.object)
        
        # make arrays out of training sets and train model for each action seperately (except for WAIT and BOMB to save time)
        for i in range(len(ACTIONS)-2):
            X_train_array = np.array(self.X_train[i])
            Y_train_array = np.array(self.Y_train[i])

            #print(x.shape, y.shape)
            self.model[i] = GradientBoostingRegressor(learning_rate=GBR_param.learning_rate, n_estimators= GBR_param.n_estimators, max_depth=GBR_param.max_depth).fit(X_train_array, Y_train_array)

        # Store the model
        with open("my-saved-model.pt", "wb") as file:
            pickle.dump(self.model, file)

        # delete the X_train, Y_train that were collected so far:
        #TODO: könnte es sein, dass der Gradientboosting regressor auch mit alten 'falscheren' daten immer weiter machen kann...? wahrscheinlich nicht..
        # Setup 6 dim array of lists to have training data for each action: X_train and Y_train
        self.X_train[:] = [],[],[],[],[],[]
        self.Y_train[:] = [],[],[],[],[],[]

        # print and reset performance
        if TEST_PERFORMANCE:
            collected_coins_mean = np.mean(np.array(self.performance)[:,0])
            steps_mean = np.mean(np.array(self.performance)[:,1])
            print("mean number of collected coins: ", collected_coins_mean)
            print("mean number of steps: ", steps_mean)
            self.performance = []


def reward_from_events(self, events: List[str]) -> int:
    """
    *This is not a required function, but an idea to structure your code.*

    Here you can modify the rewards your agent get so as to en/discourage
    certain behavior.
    """
    game_rewards = {
        e.COIN_COLLECTED: 60,
        #e.KILLED_OPPONENT: 5,
        e.INVALID_ACTION: -20,  
        #e.WAITED: -1,
        e.MOVED_UP: -1,
        e.MOVED_RIGHT: -1,
        e.MOVED_DOWN: -1,
        e.MOVED_LEFT: -1,
        #e.KILLED_SELF: -10
    }
    reward_sum = 0
    for event in events:
        if event in game_rewards:
            reward_sum += game_rewards[event]
    self.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
    return reward_sum


def auxiliary_rewards(X_t,X_tplus1):
#space for auxiliary rewards of type: F(s_t,s_tplus1) = w* phi(s_tplus1) - phi(s_t)
    #X[0:1] = (x,y to next coin)
    w = 5
    return w * np.sign(np.linalg.norm(X_t[0:2]) - np.linalg.norm(X_tplus1[0:2])) # > 0 for distance in tplus1 smaller, in [-w,w]

def append_training_data_of_symm_states(self, Y):

    # create new game states for symmetry transformations
    state = self.history[0].game_state
    s_mirror_x = dict.copy(state)
    s_mirror_y = dict.copy(state)
    s_mirror_diag = dict.copy(state)
    s_mirror_diag2 = dict.copy(state)
    s_rot = dict.copy(state)
    s_rot2 = dict.copy(state)
    s_rot3 = dict.copy(state)

    # mirror and rotate coins
    if len(self.history[0].game_state['coins']) > 0:
        coins = np.array(state['coins'])
        state['coins'] = coins.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,0],coins[:,1]], dtype = int)
        s_mirror_x['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([coins[:,0],16*np.ones(len(coins))-coins[:,1]], dtype = int)
        s_mirror_y['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([coins[:,1],coins[:,0]], dtype = int)
        s_mirror_diag['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,1],16*np.ones(len(coins))-coins[:,0]], dtype = int)
        s_mirror_diag2['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,1],coins[:,0]], dtype = int)
        s_rot['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,0],16*np.ones(len(coins))-coins[:,1]], dtype = int)
        s_rot2['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([coins[:,1],16*np.ones(len(coins))-coins[:,0]], dtype = int)
        s_rot3['coins'] = coins_transformed.T.tolist()

    # mirror and rotate others
    if len(self.history[0].game_state['others']) > 0:
        others = np.array(state['others'])

        others_transformed = np.array([16*np.ones(len(others))-others[:,0],others[:,1]], dtype = int)
        s_mirror_x['others'] = others_transformed.T.tolist()

        others_transformed = np.array([others[:,0],16*np.ones(len(others))-others[:,1]], dtype = int)
        s_mirror_y['others'] = others_transformed.T.tolist()

        others_transformed = np.array([others[:,1],others[:,0]], dtype = int)
        s_mirror_diag['others'] = others_transformed.T.tolist()

        others_transformed = np.array([16*np.ones(len(others))-others[:,1],16*np.ones(len(others))-others[:,0]], dtype = int)
        s_mirror_diag2['others'] = others_transformed.T.tolist()

        others_transformed = np.array([16*np.ones(len(others))-others[:,1],others[:,0]], dtype = int)
        s_rot['others'] = others_transformed.T.tolist()

        others_transformed = np.array([16*np.ones(len(others))-others[:,0],16*np.ones(len(others))-others[:,1]], dtype = int)
        s_rot2['others'] = others_transformed.T.tolist()

        others_transformed = np.array([others[:,1],16*np.ones(len(others))-others[:,0]], dtype = int)
        s_rot3['others'] = others_transformed.T.tolist()

    # mirror and rotate bombs
    if len(self.history[0].game_state['bombs']) > 0:
        bombs = np.array(state['bombs'])

        bombs_transformed = np.array([16*np.ones(len(bombs))-bombs[:,0],bombs[:,1]], dtype = int)
        s_mirror_x['bombs'] = bombs_transformed.T.tolist()

        bombs_transformed = np.array([bombs[:,0],16*np.ones(len(bombs))-bombs[:,1]], dtype = int)
        s_mirror_y['bombs'] = bombs_transformed.T.tolist()

        bombs_transformed = np.array([bombs[:,1],bombs[:,0]], dtype = int)
        s_mirror_diag['bombs'] = bombs_transformed.T.tolist()

        bombs_transformed = np.array([16*np.ones(len(bombs))-bombs[:,1],16*np.ones(len(bombs))-bombs[:,0]], dtype = int)
        s_mirror_diag2['bombs'] = bombs_transformed.T.tolist()

        bombs_transformed = np.array([16*np.ones(len(bombs))-bombs[:,1],bombs[:,0]], dtype = int)
        s_rot['bombs'] = bombs_transformed.T.tolist()

        bombs_transformed = np.array([16*np.ones(len(bombs))-bombs[:,0],16*np.ones(len(bombs))-bombs[:,1]], dtype = int)
        s_rot2['bombs'] = bombs_transformed.T.tolist()

        bombs_transformed = np.array([bombs[:,1],16*np.ones(len(bombs))-bombs[:,0]], dtype = int)
        s_rot3['bombs'] = bombs_transformed.T.tolist()

    # mirror and rotate self
    old_self = state['self']
    x = old_self[3][0]
    y = old_self[3][1]

    del s_mirror_x['self']
    s_mirror_x['self'] = (old_self[0], old_self[1], old_self[2], (16 - x, y))

    del s_mirror_y['self']
    s_mirror_y['self'] = (old_self[0], old_self[1], old_self[2], (x, 16 - y))

    del s_mirror_diag['self']
    s_mirror_diag['self'] = (old_self[0], old_self[1], old_self[2], (y, x))

    del s_mirror_diag2['self']
    s_mirror_diag2['self'] = (old_self[0], old_self[1], old_self[2], (16 - y, 16 - x))

    del s_rot['self']
    s_rot['self'] = (old_self[0], old_self[1], old_self[2], (16 - y, x))

    del s_rot2['self']
    s_rot2['self'] = (old_self[0], old_self[1], old_self[2], (16 - x, 16 - y))

    del s_rot3['self']
    s_rot3['self'] = (old_self[0], old_self[1], old_self[2], (y, 16 - x))

    # mirror and rotate explosion map
    explosion_map = state['explosion_map']

    s_mirror_x['explosion_map'] = np.fliplr(explosion_map)
    s_mirror_y['explosion_map'] = explosion_map.T
    s_mirror_diag['explosion_map'] = np.flipud(explosion_map)
    s_mirror_diag2['explosion_map'] = np.fliplr(np.fliplr(explosion_map).T)
    s_rot['explosion_map'] = np.fliplr(explosion_map.T)
    s_rot2['explosion_map'] = np.fliplr(np.flipud(explosion_map))
    s_rot3['explosion_map'] = np.flipud(explosion_map.T)

    # mirror and rotate field such that crates are considered
    field = state['field']
    s_mirror_x['field'] = np.fliplr(field)
    s_mirror_y['field'] = field.T
    s_mirror_diag['field'] = np.flipud(field)
    s_mirror_diag2['field'] = np.fliplr(np.fliplr(field).T)
    s_rot['field'] = np.fliplr(field.T)
    s_rot2['field'] = np.fliplr(np.flipud(field))
    s_rot3['field'] = np.flipud(field.T)

    # mirror and rotate actions
    i_action = self.history[0].i_action
    i_action_mirror_x = i_action
    i_action_mirror_y = i_action
    i_action_mirror_diag = i_action
    i_action_mirror_diag2 = i_action
    i_action_rot = i_action
    i_action_rot2 = i_action
    i_action_rot3 = i_action

    # UP
    if i_action == 0:
        # swap up down for mirroring in y
        i_action_mirror_y = 2
        # swap up left for mirroring diagonally
        i_action_mirror_diag = 3
        # swap up right for mirroring along second diagonal axis
        i_action_mirror_diag2 = 1

        # permute for the rotations
        i_action_rot = 1
        i_action_rot2 = 2
        i_action_rot3 = 3
    # RIGHT
    if i_action == 1:
        # swap right left for mirroring in x
        i_action_mirror_x = 3
        # swap down right for mirroring diagonally
        i_action_mirror_diag = 2
        # swap up right for mirroring along second diagonal axis
        i_action_mirror_diag2 = 0

        # permute for the rotations
        i_action_rot = 2
        i_action_rot2 = 3
        i_action_rot3 = 0
    # DOWN
    if i_action == 2:
        # swap up down for mirroring in y
        i_action_mirror_y = 0
        # swap down right for mirroring diagonally
        i_action_mirror_diag = 1
        # swap down left for mirroring along second diagonal axis
        i_action_mirror_diag2 = 3

        # permute for the rotations
        i_action_rot = 3
        i_action_rot2 = 0
        i_action_rot3 = 2
    # LEFT
    if i_action == 3:
        # swap right left for mirroring in x
        i_action_mirror_x = 1
        # swap up left for mirroring diagonally
        i_action_mirror_diag = 0
        # swap down left for mirroring along second diagonal axis
        i_action_mirror_diag2 = 2

        # permute for the rotations
        i_action_rot = 0
        i_action_rot2 = 1
        i_action_rot3 = 2

    # create training data out of transformed game states
    # rewards are invalid to transformations and don't have to be transformed
    self.X_train[i_action_mirror_x].append(state_to_features(s_mirror_x))
    self.Y_train[i_action_mirror_x].append(Y)

    self.X_train[i_action_mirror_y].append(state_to_features(s_mirror_y))
    self.Y_train[i_action_mirror_y].append(Y)

    self.X_train[i_action_mirror_diag].append(state_to_features(s_mirror_diag))
    self.Y_train[i_action_mirror_diag].append(Y)

    self.X_train[i_action_mirror_diag2].append(state_to_features(s_mirror_diag2))
    self.Y_train[i_action_mirror_diag2].append(Y)

    self.X_train[i_action_rot].append(state_to_features(s_rot))
    self.Y_train[i_action_rot].append(Y)

    self.X_train[i_action_rot2].append(state_to_features(s_rot2))
    self.Y_train[i_action_rot2].append(Y)

    self.X_train[i_action_rot3].append(state_to_features(s_rot3))
    self.Y_train[i_action_rot3].append(Y)
