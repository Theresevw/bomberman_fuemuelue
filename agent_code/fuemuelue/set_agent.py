from collections import namedtuple

############# SET HYPERPARAMETERS ###############

# if you start a new round of training, set this to true and a new performance.csv file is written.
# make sure to rename old performance.csv files that you want to keep!
# if you stop the training and want to continue, set this parameter to False
NEW_TRAINING = True#False

################## CALLBACKS #####################
set_policy = namedtuple('set_policy', ('name', 'train_param', 'play_param'))

# for EPSILON_GREEDY policy:
# param = epsilon
# POLICY = set_policy(name = 'EPSILON_GREEDY', train_param = 0.5, play_param = 0.05)

# for SOFTMAX policy:
# param = temperature, 0.1 is equivalent to argmax #5 leads to 2 actions ca 40 % each
POLICY = set_policy(name = 'SOFTMAX', train_param = 20, play_param = 1)

# cool down the temperature during training
COOLING=False#True

#################### TRAIN #####################

# include symmetric states in order to spped up the training
INCLUDE_SYMMETRIC_STATES = True

# print performance during training
TEST_PERFORMANCE = True

# number of rounds until the update of Q
MODEL_UPDATE_PERIOD = 120

#rewards for certain events
COIN_R= 160             # coin collected
KILL_OPP_R= 50          # killed opponend
CRATE_R=40              # destroyed crate
INVALID_R=-40           # invalid action
WAIT_R=-7               # wait
WAIT_IN_SAFETY_R = 70   # wait in a safe dead end
BOMB_R=-6               # dropped bomb
MOVE_R=-1               # moved
KILL_SELF_R=-260        # killed himself
GOT_KILL_R=-260         # got killed
NEXT_CRATE_R=70         # dropped a bomb next to a crate

# weight of the auxillary rewards
AUX_WEIGHT_COIN = 9
AUX_WEIGHT_CRATE= 6
AUX_WEIGHT_OTHER= 4
AUX_WEIGHT_BOMB = 15

# set the gradient boost modell
set_gradient_boost = namedtuple('set_gradient_boost', ('learning_rate', 'n_estimators', 'max_depth'))
GBR_param = set_gradient_boost(learning_rate=0.1, n_estimators=150, max_depth=5)

# Q leraning hyperparameters
HISTORY_SIZE = 1  # keep only 1 last transitions
GAMMA = 0.95
LEARNING_RATE = 0.1
