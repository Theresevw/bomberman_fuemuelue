import pickle
import csv
import random
from collections import namedtuple, deque
from typing import List

import numpy as np
import os
from sklearn.ensemble import GradientBoostingRegressor


import settings as s
import events as e
from .callbacks import state_to_features

#import hyperparameters
from .set_agent import NEW_TRAINING, POLICY, HISTORY_SIZE, COOLING, GAMMA,LEARNING_RATE , INCLUDE_SYMMETRIC_STATES,TEST_PERFORMANCE, MODEL_UPDATE_PERIOD,GBR_param, AUX_WEIGHT_COIN, AUX_WEIGHT_CRATE, AUX_WEIGHT_BOMB, AUX_WEIGHT_OTHER
from .set_agent import COIN_R, WAIT_IN_SAFETY_R, KILL_OPP_R, CRATE_R, INVALID_R, WAIT_R, BOMB_R, MOVE_R, KILL_SELF_R,GOT_KILL_R, NEXT_CRATE_R

# named tuple to store the game history
History = namedtuple('History',
                        ('X_t', 'i_action', 'X_tplus1', 'reward', 'game_state'))

# list of actions
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # store reward history in double ended queue
    self.history = deque(maxlen=HISTORY_SIZE)

    # Setup 6 dim array of lists to have training data for each action: X_train and Y_train
    self.X_train = np.empty(len(ACTIONS),dtype=np.object)
    self.X_train[:] = [],[],[],[],[],[]
    self.Y_train = np.empty(len(ACTIONS),dtype=np.object)
    self.Y_train[:] = [],[],[],[],[],[]
    self.end_of_round = 0

    # setup variables for performance
    self.coins_collected = 0
    self.bombs_dropped = 0
    self.crates_destroyed = 0
    self.opponents_killed = 0
    self.invalid_actions = 0
    self.waited = 0
    self.performance = []

    # load or define initial model
    if os.path.isfile("my-saved-model.pt"):
        print("Load old model for further training")
        with open("my-saved-model.pt", "rb") as file:
            self.model = pickle.load(file)

    elif os.path.isfile("initial-model.pt"):
        print("Load initial model for training")
        with open("initial-model.pt", "rb") as file:
            self.model = pickle.load(file)
    else:
        self.Q_initial = 1
        print("Start with Q_initial = ", self.Q_initial)

    # prepare file for performance if this is a new training run
    if NEW_TRAINING:
        with open('performance.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['crate density', s.CRATE_DENSITY, 'policy:', POLICY.name, 'training parameter:', POLICY.train_param, 'play parameter: ', POLICY.play_param,
                         'cooling: ', COOLING, 'update period: ', MODEL_UPDATE_PERIOD, 'weight auxiliary rewards: ', AUX_WEIGHT_COIN, AUX_WEIGHT_CRATE, AUX_WEIGHT_BOMB, AUX_WEIGHT_OTHER,
                         'GRADIENT BOOST learning rate: ', GBR_param.learning_rate, 'number of estimators: ', GBR_param.n_estimators,
                         'depth: ', GBR_param.max_depth, 'gamma', GAMMA, 'learning rate for Q-learning: ', LEARNING_RATE])
            writer.writerow(['rewards:', 'coin collected', COIN_R, 'killed opponent', KILL_OPP_R, 'crate destroyed', CRATE_R,
                            'invalid action', INVALID_R, 'wait', WAIT_R, 'placed bomb', BOMB_R, 'moved', MOVE_R,
                            'killed self', KILL_SELF_R, 'got killed', GOT_KILL_R, 'agent next to crate', NEXT_CRATE_R])
            writer.writerow(['coins', 'bombs', 'crates', 'kills', 'invalids', 'waited', 'steps', 'gotkilled', 'killedself'])
    else:
        with open('performance.csv', 'a+', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['crate density', s.CRATE_DENSITY, 'policy:', POLICY.name, 'training parameter:', POLICY.train_param, 'play parameter: ', POLICY.play_param,
                         'cooling: ', COOLING, 'update period: ', MODEL_UPDATE_PERIOD, 'weight auxiliary rewards: ', AUX_WEIGHT_COIN, AUX_WEIGHT_CRATE, AUX_WEIGHT_BOMB, AUX_WEIGHT_OTHER,
                         'GRADIENT BOOST learning rate: ', GBR_param.learning_rate, 'number of estimators: ', GBR_param.n_estimators,
                         'depth: ', GBR_param.max_depth, 'gamma', GAMMA, 'learning rate for Q-learning: ', LEARNING_RATE])
            writer.writerow(['rewards:', 'coin collected', COIN_R, 'killed opponent', KILL_OPP_R, 'crate destroyed', CRATE_R,
                            'invalid action', INVALID_R, 'wait', WAIT_R, 'placed bomb', BOMB_R, 'moved', MOVE_R,
                            'killed self', KILL_SELF_R, 'got killed', GOT_KILL_R, 'agent next to crate', NEXT_CRATE_R])
            writer.writerow(['coins', 'bombs', 'crates', 'kills', 'invalids', 'waited', 'steps', 'gotkilled', 'killedself'])
        
def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self:           This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action:    The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events:         The events that occurred when going from  `old_game_state` to `new_game_state`
    """
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    # generate training data only if not in test mode
    if new_game_state['step'] > 1 and not self.temporary_play_mode:

        # features from the old new game states
        X_t = state_to_features(old_game_state) 
        X_tplus1 = state_to_features(new_game_state)

        # rewards       
        reward = reward_from_events(events,X_t, old_game_state) + auxiliary_rewards(X_t,X_tplus1)
        
        # index for the current action
        i_action = ACTIONS.index(self_action)

        # save this step in history
        self.history.append(History(X_t, i_action, X_tplus1, reward, old_game_state))

        if old_game_state['step'] >= HISTORY_SIZE:
            # store the feature vector for the earliest state in history
            self.X_train[self.history[0].i_action].append(self.history[0].X_t)

            # compute n-step reward, equals r_t+1 for HISTORY_SIZE = 1
            reward_n_step = 0
            for k in range(HISTORY_SIZE):
                reward_n_step += self.history[k].reward * GAMMA**k

            # compute expected reward for earliest state in history
            if not os.path.isfile("my-saved-model.pt") and not os.path.isfile("initial-model.pt"):
                Y = reward_n_step + GAMMA * self.Q_initial
            else:
                if LEARNING_RATE < 1:
                    if HISTORY_SIZE == 1:
                        Q_t = self.Q[self.history[0].i_action]
                    else:
                        Q_t = self.model[self.history[0].i_action].predict(
                            self.history[0].X_t.reshape(1, len(self.history[0].X_t)))[0]
                    if self.end_of_round:
                        Y = (1 - LEARNING_RATE) * Q_t + LEARNING_RATE * reward_n_step
                    else:
                        Y = (1 - LEARNING_RATE) * Q_t + LEARNING_RATE * (
                                reward_n_step + GAMMA ** HISTORY_SIZE * maximal_Q(self, self.history[0].X_tplus1))
                else:
                    if self.end_of_round:
                        Y = reward_n_step
                    else:
                        Y = (reward_n_step + GAMMA ** HISTORY_SIZE * maximal_Q(self, self.history[0].X_tplus1))

            self.Y_train[self.history[0].i_action].append(Y)

            if INCLUDE_SYMMETRIC_STATES:
                append_training_data_of_symm_states(self, Y)

    # if in play mode, save performance, else continue
    if TEST_PERFORMANCE and self.temporary_play_mode:
        if e.COIN_COLLECTED in events:
            self.coins_collected += 1
        if e.CRATE_DESTROYED in events:
            self.crates_destroyed += 1
        if e.BOMB_DROPPED in events:
            self.bombs_dropped += 1
        if e.KILLED_OPPONENT in events:
            self.opponents_killed += 1
        if e.INVALID_ACTION in events:
            self.invalid_actions += 1
        if e.WAITED in events:
            self.waited += 1

 
def maximal_Q(self, X):
    """
    function returns the value of Q that is maximal for the best action, meaning that the estimated reward is highest
    :param self: model for Q is stored in self
    :param X: feature of the game state for which the best Q should be predicted
    :return: maximal Q
    """
    Q = np.zeros(len(ACTIONS))
    for i in range(len(ACTIONS)):
        Q[i] = self.model[i].predict(X.reshape(1,len(X)))[0]
    
    return np.max(Q)
        
    
def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.

    This is similar to reward_update. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self:             the same object that is passed to all of your callbacks.
    :param last_game_state:  a dictionary including the last game state
    :param last_action:      the action which was taken in the last step 
    """
    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')
    N_round = last_game_state['round']

    # if this is a round in play mode where we should test performance, do stuff, else continue with training
    if TEST_PERFORMANCE and self.temporary_play_mode:
        save_performance_at_end_of_round(self, last_game_state, events)

    # update model after some time interval and not if in play mode for performance testing
    if N_round % MODEL_UPDATE_PERIOD == 0 and not self.temporary_play_mode:

        # save last game state and reward for last events in training data
        self.end_of_round = 1
        game_events_occurred(self,last_game_state,last_action,last_game_state,events)
        self.end_of_round = 0

        print("update Q at round ", N_round)
        self.model = np.empty(len(ACTIONS), dtype=np.object)

        # make arrays out of training sets and train model for each action seperately (except for WAIT and BOMB to save time)
        for i in range(len(ACTIONS)):
            X_train_array = np.array(self.X_train[i])
            Y_train_array = np.array(self.Y_train[i])

            #print(x.shape, y.shape)
            self.model[i] = GradientBoostingRegressor(learning_rate=GBR_param.learning_rate, n_estimators= GBR_param.n_estimators, max_depth=GBR_param.max_depth).fit(X_train_array, Y_train_array)

        # Store the model
        with open("my-saved-model.pt", "wb") as file:
            pickle.dump(self.model, file)

        # delete the X_train, Y_train that were collected so far:
        self.X_train[:] = [],[],[],[],[],[]
        self.Y_train[:] = [],[],[],[],[],[]

        # test performance after the model was updated: change flag to start the test
        if TEST_PERFORMANCE:
            self.temporary_play_mode = True
            print("Start 15 rounds in training mode for testing performance")

    # print performance and set flag to zero to continue with training
    if len(self.performance) == 15:
        print_performance(self)
        self.temporary_play_mode = False

def reward_from_events(events: List[str], X_t, game_state) -> int:
    """
    function determines how much reward the agent gets for the list of events that occured between two game states
    this is only the reward from events, other rewards are defined in the function auxiliary_rewards
    
    :param events:     all events that happened between two time steps
    :param X_t:        list of features from the current game state
    :param game_state: the current game state
    :return:           the rewards for the events in events as an integer value
    """
    game_rewards = {
        e.COIN_COLLECTED: COIN_R,
        e.KILLED_OPPONENT: KILL_OPP_R,
        e.CRATE_DESTROYED: CRATE_R,
        e.INVALID_ACTION: INVALID_R,  
        e.WAITED: WAIT_R,
        e.BOMB_DROPPED: BOMB_R,
        e.MOVED_UP: MOVE_R,
        e.MOVED_RIGHT: MOVE_R,
        e.MOVED_DOWN: MOVE_R,
        e.MOVED_LEFT: MOVE_R,
        e.KILLED_SELF: KILL_SELF_R,
        e.GOT_KILLED: GOT_KILL_R,
    }
    reward_sum = 0
    for event in events:
        if event in game_rewards:
            reward_sum += game_rewards[event]
    
    if e.BOMB_DROPPED in events:
        (x,y)=game_state['self'][3]
        
        # reward, when crate or opponent is on a neighboured tile
        if (X_t[-1] == 1  or X_t[-4] == 1):
            reward_sum = reward_sum + NEXT_CRATE_R
          
        # punish, when no crates and opponent is on a neighboured tile
        if (X_t[-1] == 0 and X_t[-4] == 0):
            reward_sum = reward_sum - NEXT_CRATE_R

    # reward him for waiting if this is a safe spot and all other movements are bad
    if e.WAITED in events and X_t[11] == 0:
        if X_t[0] > 0 and X_t[1] > 0 and X_t[2] > 0 and X_t[3] > 0:
            #print("he waited in safety, yeah!")
            reward_sum += WAIT_IN_SAFETY_R

    return reward_sum

def auxiliary_rewards(X_t,X_tplus1):
    """
    function that computes the auxilary rewards with the features from the last and 
    the new game state.
    
    :param X_t:            list of features from the last game state 
    :param X_tplus1:       list of features from the next game state 
    :return:               int, the auxilary reward
    """

    # decrease distance to coin
    coin_distance=(np.linalg.norm(X_t[4:6]) -np.linalg.norm(X_tplus1[4:6])) 
    
    # decrease distance to next crater
    crater_distance=(np.linalg.norm(X_t[6:8]) - np.linalg.norm(X_tplus1[6:8]))
    
    # decrease distance to others
    other_distance=(np.linalg.norm(X_t[8:10]) - np.linalg.norm(X_tplus1[8:10]))
    
    # decrease level of danger 
    bomb_distance = (X_t[-3] - X_tplus1[-3])
    
    # in our case taking the euclidian norm with the sign function is just the same 
    # like taking the 1norm of the distance vectors, because the agent can maximal move one integer
    # value, so that only one entry of each distance vector can be add with 1 or -1 
    # so the difference between the 1norms of the distance vector will be 1, 0 or -1
    # depending, if the ditance decrased, increased or remained the same
    return AUX_WEIGHT_COIN * np.sign(coin_distance)+ AUX_WEIGHT_CRATE * np.sign(crater_distance) + AUX_WEIGHT_OTHER * np.sign(other_distance)+ AUX_WEIGHT_BOMB * bomb_distance

def save_performance_at_end_of_round(self, last_game_state, events):
    """
    function that saves the agent's performance for immediate feedback during training
    and resets the parameters that count the relevant events
    
    :param self:            contains a list for the round performances
    :param last_game_state: contains the last step of the round
    :param events:          last events to find out why the game ended
    """

    got_killed = 0
    killed_self = 0
    if e.GOT_KILLED in events:
        got_killed += 1
    if e.KILLED_SELF in events:
        killed_self += 1

    current_performance_list = [self.coins_collected, self.bombs_dropped, self.crates_destroyed, self.opponents_killed,
         self.invalid_actions/last_game_state['step'], self.waited/last_game_state['step'], last_game_state['step'], got_killed, killed_self]

    # save performance as list element in self.performance
    self.performance.append(np.array(current_performance_list))

    # save performance in file as well
    with open('performance.csv', 'a+', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(current_performance_list)

    # reset counters such that they start at zero at next game
    self.coins_collected = 0
    self.bombs_dropped = 0
    self.crates_destroyed = 0
    self.opponents_killed = 0
    self.invalid_actions = 0
    self.waited = 0

def print_performance(self):
    """
    every time the model is updated, we generate an output that should show how the agent performed
    
    :param self: contains the performance of all rounds that were played with the current model
    """

    # calculate average number of coins, dropped bombs, etc.
    mean_performance = np.mean(np.array(self.performance),0)

    # save model into another file if the agent doesn't kill himself in 10 games
    if mean_performance[7] < 0.01:
        with open("agent_does_not_kill_himself_model.pt", "wb") as file:
            pickle.dump(self.model, file)
        with open('agent_does_not_kill_himself_mean_performance.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(mean_performance)
        with open('performance.csv','a+',newline='') as file:
            writer = csv.writer(file)
            writer.writerow("at this point the model is saved in agent_does_not_kill_himself_model.pt")

    # print relevant information
    print("number of events per round averaged over ", 15, " rounds:")
    print("collected coins: ", np.round(mean_performance[0],2))
    print("dropped bombs: ", np.round(mean_performance[1],2))
    print("destroyed crates: ", np.round(mean_performance[2],2))
    print("killed opponents: ", np.round(mean_performance[3],2))

    print("\nmean percentage of invalid actions: ", np.round(mean_performance[4],2))
    print("mean percentage of action wait:", np.round(mean_performance[5],2))

    print("\nOn average, games end in round: ", np.round(mean_performance[6],2))
    print("ratio of games that end because agent got killed: ",
          np.round(mean_performance[7], 2))
    print("ratio of games that end because agent killed itself: ",
          np.round(mean_performance[8], 2))

    # reset performance list to empty list, deleting all performances of the current model
    self.performance = []

def append_training_data_of_symm_states(self, Y):

    # create new game states for symmetry transformations
    state = self.history[0].game_state
    s_mirror_x = dict.copy(state)
    s_mirror_y = dict.copy(state)
    s_mirror_diag = dict.copy(state)
    s_mirror_diag2 = dict.copy(state)
    s_rot = dict.copy(state)
    s_rot2 = dict.copy(state)
    s_rot3 = dict.copy(state)

    # mirror and rotate coins
    if len(self.history[0].game_state['coins']) > 0:
        coins = np.array(state['coins'])
        state['coins'] = coins.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,0],coins[:,1]], dtype = int)
        s_mirror_x['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([coins[:,0],16*np.ones(len(coins))-coins[:,1]], dtype = int)
        s_mirror_y['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([coins[:,1],coins[:,0]], dtype = int)
        s_mirror_diag['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,1],16*np.ones(len(coins))-coins[:,0]], dtype = int)
        s_mirror_diag2['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,1],coins[:,0]], dtype = int)
        s_rot['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([16*np.ones(len(coins))-coins[:,0],16*np.ones(len(coins))-coins[:,1]], dtype = int)
        s_rot2['coins'] = coins_transformed.T.tolist()

        coins_transformed = np.array([coins[:,1],16*np.ones(len(coins))-coins[:,0]], dtype = int)
        s_rot3['coins'] = coins_transformed.T.tolist()

    # mirror and rotate bombs
    if len(state['bombs']) > 0:
        s_mirror_x['bombs'] = []
        s_mirror_y['bombs'] = []
        s_mirror_diag['bombs'] = []
        s_mirror_diag2['bombs'] = []
        s_rot['bombs'] = []
        s_rot2['bombs'] = []
        s_rot3['bombs'] = []

        for i in range(len(state['bombs'])):
            (x,y),t = state['bombs'][i]
            s_mirror_x['bombs'].append(((16 - x,y),t))
            s_mirror_y['bombs'].append(((x,16 - y),t))
            s_mirror_diag['bombs'].append(((y,x),t))
            s_mirror_diag2['bombs'].append(((16 - y,16 - x),t))
            s_rot['bombs'].append(((16 - y,x),t))
            s_rot2['bombs'].append(((16 - x,16 - y),t))
            s_rot3['bombs'].append(((y,16 - x),t))

    # mirror and rotate self
    (n, s, b, (x, y)) = state['self']

    del s_mirror_x['self']
    s_mirror_x['self'] = (n, s, b, (16 - x, y))

    del s_mirror_y['self']
    s_mirror_y['self'] = (n, s, b, (x, 16 - y))

    del s_mirror_diag['self']
    s_mirror_diag['self'] = (n, s, b, (y, x))

    del s_mirror_diag2['self']
    s_mirror_diag2['self'] = (n, s, b, (16 - y, 16 - x))

    del s_rot['self']
    s_rot['self'] = (n, s, b, (16 - y, x))

    del s_rot2['self']
    s_rot2['self'] = (n, s, b, (16 - x, 16 - y))

    del s_rot3['self']
    s_rot3['self'] = (n, s, b, (y, 16 - x))

    # mirror and rotate others
    if len(state['others']) > 0:
        s_mirror_x['others'] = []
        s_mirror_y['others'] = []
        s_mirror_diag['others'] = []
        s_mirror_diag2['others'] = []
        s_rot['others'] = []
        s_rot2['others'] = []
        s_rot3['others'] = []

        for i in range(len(state['others'])):
            (n, s, b, (x,y)) = state['others'][i]
            s_mirror_x['others'].append((n, s, b, (16 - x,y)))
            s_mirror_y['others'].append((n, s, b, (x,16 - y)))
            s_mirror_diag['others'].append((n, s, b, (y,x)))
            s_mirror_diag2['others'].append((n, s, b, (16 - y,16 - x)))
            s_rot['others'].append((n, s, b, (16 - y,x)))
            s_rot2['others'].append((n, s, b, (16 - x,16 - y)))
            s_rot3['others'].append((n, s, b, (y,16 - x)))

    # mirror and rotate explosion map
    explosion_map = state['explosion_map']
    s_mirror_x['explosion_map'] = np.flipud(explosion_map)
    s_mirror_y['explosion_map'] = np.fliplr(explosion_map)
    s_mirror_diag['explosion_map'] = explosion_map.T
    s_mirror_diag2['explosion_map'] = np.fliplr(np.fliplr(explosion_map).T)
    s_rot['explosion_map'] = np.fliplr(explosion_map).T
    s_rot2['explosion_map'] = np.fliplr(np.flipud(explosion_map.T)).T
    s_rot3['explosion_map'] = np.flipud(explosion_map).T

    # mirror and rotate field such that crates are considered
    field = state['field']
    s_mirror_x['field'] = np.flipud(field)
    s_mirror_y['field'] = np.fliplr(field)
    s_mirror_diag['field'] = field.T
    s_mirror_diag2['field'] = np.fliplr(np.fliplr(field).T)
    s_rot['field'] = np.fliplr(field).T
    s_rot2['field'] = np.fliplr(np.flipud(field.T)).T
    s_rot3['field'] = np.flipud(field).T

    # mirror and rotate actions
    i_action = self.history[0].i_action
    i_action_mirror_x = i_action
    i_action_mirror_y = i_action
    i_action_mirror_diag = i_action
    i_action_mirror_diag2 = i_action
    i_action_rot = i_action
    i_action_rot2 = i_action
    i_action_rot3 = i_action

    # UP
    if i_action == 0:
        # swap up down for mirroring in y
        i_action_mirror_y = 2
        # swap up left for mirroring diagonally
        i_action_mirror_diag = 3
        # swap up right for mirroring along second diagonal axis
        i_action_mirror_diag2 = 1

        # permute for the rotations
        i_action_rot = 1
        i_action_rot2 = 2
        i_action_rot3 = 3
    # RIGHT
    if i_action == 1:
        # swap right left for mirroring in x
        i_action_mirror_x = 3
        # swap down right for mirroring diagonally
        i_action_mirror_diag = 2
        # swap up right for mirroring along second diagonal axis
        i_action_mirror_diag2 = 0

        # permute for the rotations
        i_action_rot = 2
        i_action_rot2 = 3
        i_action_rot3 = 0
    # DOWN
    if i_action == 2:
        # swap up down for mirroring in y
        i_action_mirror_y = 0
        # swap down right for mirroring diagonally
        i_action_mirror_diag = 1
        # swap down left for mirroring along second diagonal axis
        i_action_mirror_diag2 = 3

        # permute for the rotations
        i_action_rot = 3
        i_action_rot2 = 0
        i_action_rot3 = 2
    # LEFT
    if i_action == 3:
        # swap right left for mirroring in x
        i_action_mirror_x = 1
        # swap up left for mirroring diagonally
        i_action_mirror_diag = 0
        # swap down left for mirroring along second diagonal axis
        i_action_mirror_diag2 = 2

        # permute for the rotations
        i_action_rot = 0
        i_action_rot2 = 1
        i_action_rot3 = 2

    # create training data out of transformed game states
    # rewards are invalid to transformations and don't have to be transformed
    self.X_train[i_action_mirror_x].append(state_to_features(s_mirror_x))
    self.Y_train[i_action_mirror_x].append(Y)

    self.X_train[i_action_mirror_y].append(state_to_features(s_mirror_y))
    self.Y_train[i_action_mirror_y].append(Y)

    self.X_train[i_action_mirror_diag].append(state_to_features(s_mirror_diag))
    self.Y_train[i_action_mirror_diag].append(Y)

    self.X_train[i_action_mirror_diag2].append(state_to_features(s_mirror_diag2))
    self.Y_train[i_action_mirror_diag2].append(Y)

    self.X_train[i_action_rot].append(state_to_features(s_rot))
    self.Y_train[i_action_rot].append(Y)

    self.X_train[i_action_rot2].append(state_to_features(s_rot2))
    self.Y_train[i_action_rot2].append(Y)

    self.X_train[i_action_rot3].append(state_to_features(s_rot3))
    self.Y_train[i_action_rot3].append(Y)
